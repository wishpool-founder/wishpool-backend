var express = require('express');
var session = require('express-session');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var multer = require('multer');
var cors = require('cors');

var fs = require("fs");
var Promise = require("promise");
var concat = require("concat-stream");


function controllers(params)
{
  	var app = params.app;
  	var db = params.db;   	

	var collectionName = "wishpool_activities";     

	
  	////////////////////////////////////////////////////////////////////////////////
	/// AJAX services:
	////////////////////////////////////////////////////////////////////////////////
	app.get('/activities', function(req, res) {	  	  	  	  
			var route = db.route('wishpool/activities');
			route.get(function (err, result) {
			    if (err) return console.error(err);
			    // result is the response body of calling
			    // GET _db/_system/my-foxx-app			    
			    res.send(result.rawBody);
			});	  				   	  
  	}); 
	
	app.get("/activities/:key", function (req, res) {	  
	  if (global.sess) {
	  	  	var key = req.params["key"];	  	  
	  	  	var url = 'wishpool/activities/' + key;
		  	var route = db.route(url);
			route.get(function (err, result) {
			    if (err) return console.error(err);
			    // result is the response body of calling
			    // GET _db/_system/my-foxx-app			    
			    console.log(result);
			    res.send(result.rawBody);
			});	 
	  } else {
	  		res.json({msg: "invalid user"});
	  }
	  
	});	
	
	app.put("/activities/put", function (req, res) {
		// This is just a trampoline to the Foxx app:
		var putRoute = "_db/_system/wishpool/activities";	// configure activities collection	
		ep = db.route(putRoute);

		req.pipe(concat( function(body) {
			// check out body-parser for a express middleware which handles json automatically
			ep.put("put", JSON.parse(body.toString()),
				function(err, x) {
					if (err) {
					  	err.error = true;
					 	res.send(err);
					}
					else {
					  	res.send(x);
					}
				});
		} ));
	});

	app.post("/activities/joinActivity", function (req, res) {				
		// This is just a trampoline to the Foxx app:
		var eq = db.route('wishpool');		

		req.pipe(concat( function(body) {
			// check out body-parser for a express middleware which handles json automatically		
			
			console.log(req.body);
			var url = "activities/" + req.body._key;
			console.log(url);
			eq.put(url, req.body,
				function(err, x) {
					if (err) {					  	
					 	console.log('error: %j', err);
					 	res.json({msg: "failed"});
					}
					else {					  	
					  	res.json({msg: "success"});
					}
				});
		} ));
		
	});

	app.get("/activities/myactivities/:uid", function (req, res) {					  
	  if(global.sess)
	  {
			var uid = req.params["uid"];
			var route = db.route('wishpool/activities');
			var myactivities = [];

			route.get(function (err, result) {
			    if (err) return console.error(err);
			    // result is the response body of calling
			    // GET _db/_system/my-foxx-app			    			    	    
			    var activities = JSON.parse(result.rawBody.toString());
				for (var i = 0; i < activities.length; i++) {
	              	var activity = activities[i];
	              	console.log(activity);
	              	console.log("\n");
	              	for (var j = 0; j < activity.members.length; j++) {
	              		if (activity.members[j] == uid) {
	            			myactivities.push(activity);				      			
	            			break;
	              		};
	              	};
	            };
	            res.json(myactivities);	            
			});
	  }else{
			res.json({msg: "invalid user"});
	  }	  	  
	});
}

module.exports = controllers;