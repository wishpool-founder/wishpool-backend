var express = require('express');
var session = require('express-session');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var multer = require('multer');
var cors = require('cors');

var fs = require("fs");
var Promise = require("promise");
var concat = require("concat-stream");

function controllers(params)
{
  	var app = params.app;
  	var db = params.db;  	  	
  	var route;
  	
  	
  	////////////////////////////////////////////////////////////////////////////////
	/// AJAX services:
	////////////////////////////////////////////////////////////////////////////////
	app.get('/users', function(req, res, next) {
		sess = req.session;
		res.send('respond with a users resource');
  	}); 
	
	app.get("/users/:key", function (req, res) {
  		var key = req.params["key"];	  	  
  	  	var url = 'wishpool/users/' + key;
	  	var route = db.route(url);
		route.get(function (err, result) {
		    if (err) {
		    	res.send("404");
		    	return ;
		    }
		    // result is the response body of calling
		    // GET _db/_system/my-foxx-app			    
		    //console.log(result);
		    res.json(result.rawBody);
		});	 
	});	

	app.post("/users", function (req, res) {			
		route = db.route('wishpool');
		console.log(req.body);
		req.pipe(concat( function() {			
			route.post("users", req.body, function (err, result) {
				    if (err) { 
				    	res.json(err.code);
				    	return console.error(err.code); 
				    }
				    // result is the response body of calling
				    // POST _db/_system/my-foxx-app/users
				    // with JSON request body {"username": "admin", "password": "hunter2"}		    
					res.json({msg: "success"});
			});		
		} ));
	});

	app.post("/users/signup", function (req, res) {			
		route = db.route('wishpool');
		console.log(req.body);
		req.pipe(concat( function() {			
			route.post("users", req.body, function (err, result) {
				    if (err) { 
				    	res.json(err.code);
				    	return console.error(err.code); 
				    }
				    // result is the response body of calling
				    // POST _db/_system/my-foxx-app/users
				    // with JSON request body {"username": "admin", "password": "hunter2"}		    
				    global.sess = req.body._key;
					res.json({msg: "success"});
			});		
		} ));
	});

	app.post("/users/login", function (req, res) {			
		var key = req.body._key;		
	  	var url = 'wishpool/users/' + key;
	  	var route = db.route(url);
		route.get(function (err, result) {
		    if (err) return console.error(err);
		    // result is the response body of calling
		    // GET _db/_system/my-foxx-app			    
		    console.log(result);
		    
		    if (result.rawBody.password == req.body.password) {
		    	//set email into the session.
          	  	global.sess = key;
          	  	console.log(global.sess);
          	  	res.json({msg: "loggedin"});	
		    } else{
		    	res.json({msg: "failed"});
		    };
		});	 	  	
	});

	app.post("/users/signout", function (req, res) {					
		console.log("sign out")
	  	if (global.sess) {
	  		delete global.sess;
	  		console.log("signed out");
	  		res.json({msg: "signed out"});
	  	} else{
	  		res.json({msg: "failed to sign out"});
	  	};
	});
}

module.exports = controllers;