'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */
angular.module('sbAdminApp')
    .directive('stats',function() {
    	return {
  		templateUrl:'scripts/directives/stats/stats.html',
  		restrict:'E',
  		replace:true,
  		scope: {
        'model': '=',        
        'name': '@',        
        'colour': '@'   
  		}
  		
  	}
  });
