'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */
angular.module('sbAdminApp')
	.directive('loginHeaderNotification',function(){
		return {
        templateUrl:'scripts/directives/login-header/header-notification/header-notification.html',
        restrict: 'E',
        replace: true,
    	}
	});


