'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */
angular.module('sbAdminApp')
	.directive('loginHeader',function(){
		return {
        templateUrl:'scripts/directives/login-header/header.html',
        restrict: 'E',
        replace: true,
    	}
	});


