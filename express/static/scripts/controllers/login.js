'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # LoginCtrl
 * Controller of the sbAdminApp
 */
angular.module('sbAdminApp').controller('LoginCtrl', ['$scope', '$http', 'sharedService', '$window',
    function ($scope, $http, sharedService, $window) {
        // redirect to profile if user is logged in
        var logged = sharedService.checkLogin();
        if (logged) {
            $window.location.href = '#/dashboard/activities'
        }
        //initializing user object
        $scope.user = {
            email: "",
            pass: ""
        };
        
        // sign in temporary function
        $scope.logIn = function () {            
            var user = {
                "_key": $scope.user.email,
                "pass": $scope.user.pass
            };
            $http.post('http://localhost:3000/users/login', user).
                        success(function (data, status, headers, config) {
                            console.log(data);
                            if (data.msg == "loggedin") {
                                sharedService.registerLoggedUser(user);
                                $window.location.href = '#/dashboard/activities'
                            } else{
                                alert("Failed to login")
                            };
                            
                        }).
                        error(function (data, status, headers, config) {
                            console.log(data)
                            alert("User not found")
                        })                          
        }
    }]);
