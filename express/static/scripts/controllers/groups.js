'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:dMainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module('sbAdminApp').controller('groupCtrl', ['$scope', '$http', 'sharedService',
    function ($scope, $http, sharedService) {                                    	        
        
        var checkingUserExistanceInGroups = [];
        var currentUser = JSON.parse(sharedService.checkLogin());


        var myGroups = [];
        var restGroups = [];
        
        $http.get('http://localhost:3000/groups').
            success(function (data) {            	
                $scope.groups = data;                                 
                for (var i = 0; i < data.length; i++) {                	
            		for (var j = 0; j < data[i].members.length; j++) {            			
            			if (currentUser._key == data[i].members[j]) {
            				data[i].userFound = 1;
            				myGroups.push(data[i]);            			
            			}
            		};
            		if (! $scope.groups[i].userFound) {
            			restGroups.push(data[i]);            			
            		};
            	};          

            	$scope.myGroups = myGroups;
            	$scope.restGroups = restGroups;
            })    

        $scope.joinGroup = function (item) {
               		
               		var userid = currentUser._key;      
               		var groupid = item._key;         		
               		item.members.push(userid);
               		console.log(item);               		              			                

	                $http.post('http://localhost:3000/groups/joinGroup', item).
	                            success(function (data, status, headers, config) {
	                                console.log(data);   
	                                if (data.msg == "success") {
	                                	alert("Success to join group");
	                                	item.userFound = 1;
                                        $scope.myGroups.push(item);
                                        var index = restGroups.indexOf(item);
                                        $scope.restGroups.splice(index,1);
	                                } else{
	                                	alert("Failed to join group");
	                                };
	                                
	                            }).
	                            error(function (data, status, headers, config) {
	                                console.log(data)	                                
	                            })      

               }  

    }
]);

