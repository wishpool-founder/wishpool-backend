angular.module('sbAdminApp').controller('sharedCtrl', ['$scope', '$window', '$http', 'sharedService',
    function ($scope, $window, $http, sharedService) {       
        $scope.signOut = function () {
            $http.post('http://localhost:3000/users/signout', {}).
                            success(function (data, status, headers, config) {
                                console.log(data);
                                if (data.msg == "signed out") {
                                    localStorage.removeItem('wishpoolUser');
                                    $window.location.href = '#/'                                                            
                                } else{
                                    //alert("Failed to sign out")
                                    localStorage.removeItem('wishpoolUser');
                                    $window.location.href = '#/'
                                    
                                };
                                
                            }).
                            error(function (data, status, headers, config) {
                                console.log(data)
                                localStorage.removeItem('wishpoolUser');
                                $window.location.href = '#/'
                                alert("Internal server error")
                            })               
            
        }
    }]);