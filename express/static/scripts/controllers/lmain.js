'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module('sbAdminApp').controller('lMainCtrl', ['$scope', '$http', 'sharedService',
    function ($scope, $http, sharedService) {  
        $http.get('http://localhost:3000/groups').
            success(function (data) {
            	console.log(data);
                $scope.groups = data;                                          
            })                  
        $http.get('http://localhost:3000/activities').
            success(function (data) {
            	console.log(data);
                $scope.activities = data;                                          
            })     
    }
]);
