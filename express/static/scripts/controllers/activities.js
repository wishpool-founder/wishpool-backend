'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:dMainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module('sbAdminApp').controller('activityCtrl', ['$scope', '$http', 'sharedService',
    function ($scope, $http, sharedService) {                                    	                
        var currentUser = JSON.parse(sharedService.checkLogin());


        var myActivities = [];
        var restActivities = [];
        
        $http.get('http://localhost:3000/activities').
            success(function (data) {            	
                $scope.activities = data;                                 
                console.log($scope.activities);
                for (var i = 0; i < data.length; i++) {                	
                	console.log(i);
            		for (var j = 0; j < data[i].members.length; j++) {            			
            			if (currentUser._key == data[i].members[j]) {
            				data[i].userFound = 1;
            				myActivities.push(data[i]);            			
            			}
            		};
            		if (! $scope.activities[i].userFound) {
            			restActivities.push(data[i]);            			
            		};
            	};          

            	$scope.myActivities = myActivities;
            	$scope.restActivities = restActivities;
            })                   

        $scope.joinActivity = function (item) {               		
               		var userid = currentUser._key;      
               		var activity_id = item._key;         		
               		item.members.push(userid);
               		console.log(item);               		              			                

	                $http.post('http://localhost:3000/activities/joinActivity', item).
	                            success(function (data, status, headers, config) {
	                                console.log(data);   
	                                if (data.msg == "success") {
	                                	alert("Success to join activity");
	                                	item.userFound = 1;
                                        $scope.myActivities.push(item);
                                        var index = restActivities.indexOf(item);
                                        $scope.restActivities.splice(index,1);
	                                } else{
	                                	alert("Failed to join activity");
	                                };
	                                
	                            }).
	                            error(function (data, status, headers, config) {
	                                console.log(data)	                                
	                            })      

               }  

    }
]);