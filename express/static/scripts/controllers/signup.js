'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # SignupCtrl
 * Controller of the sbAdminApp
 */

angular.module('sbAdminApp').controller('SignupCtrl',['$scope', '$http', '$window','sharedService',
	function ($scope, $http, $window, sharedService) {

		$scope.signUp = function () {

        // check the existence of this user
      $http.get('http://localhost:3000/users/' + $scope.user.email).
            success(function (data, status, headers, config) {
                // if this is a current user create a session with user email and redirect to it's profile
                console.log(data);
                //sharedService.registerLoggedUser(data);
                if (data == "404") {
                    // if it is a new user add it to the database then redirect it to it's profile
                    console.log(data);
                    var newUser={
                        "_key": $scope.user.email,
                        "name": $scope.user.name,                            
                        "email": $scope.user.email,
                        "password": $scope.user.pass
                    }

                    console.log(newUser);
                    
                    $http.post('http://localhost:3000/users/signup', newUser).
                        success(function (data, status, headers, config) {                                                           
                            console.log(data);
                            if (data.msg == "success") {
                                alert("success to register");
                                sharedService.registerLoggedUser(newUser);
                                $window.location.href = '#/dashboard/activities'
                                return;
                            } else {
                                alert("Failed to register");
                                return;
                            }
                            
                        }).
                        error(function (data, status, headers, config) {
                            console.log(data);                            
                            return;
                        })
                } else{
                    alert("user already exist");
                    return;    
                };                        
            }).
            error(function (data, status, headers, config) {
                
            });

  		}

}]);

