var fs = require('fs');
var http = require('http');
var config = require('./config')();
var _ = require('lodash');
var queryString = require('querystring');
var jaccard = require('jaccard');

var options = {
            host: 'localhost',
            port: 8529,
            path: '/_db/_system/wishpool/groups',
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',                
            }
};


var req = http.request(options, function (res) {
    res.setEncoding('utf8');
    res.on('data', function (data) {
        //console.log("body: " + data);        
        var obj = JSON.parse(data);
        if (obj.length) {
            console.log("groups exists");
        } else{         
            console.log("groups and activities doesn`t exist! init groups and activities!");  
            /* inist groups */
            fs.readFile('seedData/seed_groups.json', 'utf8', function (err, data) {
                if (err) throw err;
                var groups_obj = JSON.parse(data);      

                /* get linked groups */
                linkGroups(groups_obj);

                for (var i = 0; i < groups_obj.length ; i++) {
                    var data = JSON.stringify(groups_obj[i]);        
                    var options = {
                        host: 'localhost',
                        port: 8529,
                        path: '/_db/_system/wishpool/groups',
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                            'Content-Length': Buffer.byteLength(data)
                        }
                    };
                    var req = http.request(options, function (res) {
                        res.setEncoding('utf8');
                        res.on('data', function (data) {
                            //console.log("body: " + data);
                        });
                    });
                    req.write(data);
                    req.end();
                };
            });

            /* init activities */
            fs.readFile('seedData/seed_activities.json', 'utf8', function (err, data) {
                if (err) throw err;
                var activities_obj = JSON.parse(data);      

                /* get linked activities */
                linkActivities(activities_obj);

                for (var i = 0; i < activities_obj.length ; i++) {
                    var data = JSON.stringify(activities_obj[i]);        
                    var options = {
                        host: 'localhost',
                        port: 8529,
                        path: '/_db/_system/wishpool/activities',
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                            'Content-Length': Buffer.byteLength(data)
                        }
                    };
                    var req = http.request(options, function (res) {
                        res.setEncoding('utf8');
                        res.on('data', function (data) {
                            //console.log("body: " + data);
                        });
                    });
                    req.write(data);
                    req.end();
                };
            });

        };
    });
});

req.write("");
req.end();

function linkGroups( groups_obj ) {
    for (var i = 0; i < groups_obj.length ; i++) {
        for (var j = i+1; j < groups_obj.length; j++) {
                    
            /* compare two groups.  */
            var a = groups_obj[i].aboutUs;
            var b = groups_obj[j].aboutUs;                          
            
            var simuliraty = jaccard.index(a, b)                          
            /* decide to link or not */
            if (simuliraty > config.big_enough) {
                groups_obj[i].links.push(groups_obj[j].no);
                groups_obj[j].links.push(groups_obj[i].no);
            };
        };        
    }       
}


function linkActivities( activities_obj ) {
    for (var i = 0; i < activities_obj.length ; i++) {
        for (var j = i+1; j < activities_obj.length; j++) {
                    
            /* compare two groups.  */
            var a = activities_obj[i].tabs;
            var b = activities_obj[j].tabs;                          
            
            var simuliraty = jaccard.index(a, b)                          
            /* decide to link or not */
            if (simuliraty > config.big_enough) {
                activities_obj[i].links.push(activities_obj[j].no);
                activities_obj[j].links.push(activities_obj[i].no);
            };
        };        
    }       
}


  